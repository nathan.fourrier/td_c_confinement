#include <stdio.h>

void fois_deux(int* n) {
    (*n) = 2 * (*n);
}

void divise_deux(int* n) {
    (*n) = (*n) / 2;
}

void printINT(int* var) {
    printf("%d\n",*var);
}



void apply(int* tab, int size, void (*func)(int*)) {
    for(int i=0; i<size; i++) {
        func(tab + i); //get the element adress starting by the array pointer
    }
}


