#include <stdio.h>
#include "functions.h"

int main() {
    
    int myArray[] = {0, 14, 5, 36};
    
    printf("Tableau de base : \n");
    apply(myArray, 4, printINT);
    
    apply(myArray, 4, fois_deux);
    
    printf("Tableau après multiplication par deux : \n");
    apply(myArray, 4, printINT);
    
    apply(myArray, 4, divise_deux);
    
    printf("Tableau après division par deux : \n");
    apply(myArray, 4, printINT);
    
    
    return 0;
}
