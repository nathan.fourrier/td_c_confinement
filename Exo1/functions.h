//Function wich replacing the value of an int by its double (need to give pointer of the int)
void fois_deux(int* n);

//Function wich replacing the value of an int by its half (need to give pointer of the int)
void divise_deux(int* n);

//Print an int to console giving its pointer
void printINT(int* var);

//Apply a function to a tab 
void apply(int* tab, int size, void (*func)(int*));


