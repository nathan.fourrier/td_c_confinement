/**
* struct: element_
* ----------------------------
*   Element of doubly linked list
*
* @ prev
    pointer to the previous element
*
* @ next
    pointer to the next element
*
* @ value
    value of the element 
*
*/
struct element_ {
    struct element_ * prev;
    struct element_ * next;
    int value;
};

typedef struct element_* element;

/**
* struct: list_
* ----------------------------
*   Doubly linked list
*
* @ first
    pointer to the first element of the list
*
* @ last
    pointer to the last element of the list
*

*/
struct list_ {

    element first;
    element last;
};

typedef struct list_* list;





/**
* Function: createElement
* ----------------------------
*   Create a new Element to use in list 
*   Next and Prev are initialized to NULL
*
* @param value
    the int value to init in the element 
*
* @return pointer to the new element 
*/
element createElement(int value);

//function return a new list contain 1 element (take the init value in input)
/**
* Function: createList
* ----------------------------
*   Create a new list with 1 element
*
* @param value
    the int value to init in the first element 
*
* @return pointer to the list 
*/
list createList(int value);

/**
* Function: listSize
* ----------------------------
*   Count the number of elements in a list
*
* @param myList
    the list to count
*
* @return the size of the list
*/
int listSize(list myList);


/**
* Function: listAppend
* ----------------------------
*   Append a new element at the end of a list
*
* @param myList
    the list which to append an element 
*
* @param value
    the value of the new element
*
*/
void listAppend(list myList, int value);


/**
* Function: listRemoveElement
* ----------------------------
*   Remove an element of a list (and free the memory)
*   If the position don't exist, the function does nothing
*
* @param myList
    the list which to remove an element 
*
* @param position
    the position of the element to delete
*
*/
void listRemoveElement(list myList, int position);


/**
* Function: printList
* ----------------------------
*   print the values of the elements in list
*
*/
void printList(list myList);


//delete and free list
/**
* Function: freeList
* ----------------------------
*   Remove and free all elements and the list
*
* @param myList
    the list to free
*
*/
void freeList(list myList);


