#define NULL ((void*)0)
#include <stdio.h>
#include "functions.h" 
//isOdd
int isOdd(int number) {
    return ((number % 2) != 0);
}

//isPrime
int isPrime(int number) {
    
    if (number % 2 == 0) {
        return (number == 2);
    }
    for(int i=3; i * i <= number; i +=2) {
        if (number % i == 0) {
          return 0;
        }
    }
    return 1;
}

//isNegative
int isNegative(int number) {

    return number<0;
}


//
void removeWhere(list myList, int (*condition)(int)) {
    
    int pos =0;
    element elm = myList->first;
    
    while(elm!=NULL) {
        if(condition(elm->value)) {
            
            //back into the previous element before free the current element
            elm = elm->prev;
            
            listRemoveElement(myList,pos);
            pos--;
            //if the value to remove was the first
            if(pos==-1) { 
            elm = myList->first;
            } 
        }
        
        if(pos == -1){
            elm = myList->first;
        }
        else if (elm!=NULL){   
            elm = elm->next;   
        }
        pos++;
    }
}

