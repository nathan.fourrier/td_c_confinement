#include "list.h" //librairie listes chaînées de mon TP 
//return 1 if value is odd
int isOdd(int number);

//return 1 if value is a prime number
int isPrime(int number);

//return 1 if value is negative
int isNegative(int number);

//remove values wich validate the condition fonction
void removeWhere(list myList, int (*condition)(int));

