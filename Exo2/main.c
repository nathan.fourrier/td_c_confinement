
#include <stdio.h>
#include <stdlib.h>

#include "functions.h"

int main() {
    list liste_1 = createList(-5);
    listAppend(liste_1, -11);
    listAppend(liste_1, 3);
    listAppend(liste_1, 26);
    listAppend(liste_1, 4);
    listAppend(liste_1, -2);
    listAppend(liste_1, 4);
    listAppend(liste_1, -8);
    listAppend(liste_1, 9);
    listAppend(liste_1, -4);

    
    printList(liste_1);
    

    printf("\n\nRetrait des nombres premiers\n\n");
    removeWhere(liste_1, isPrime);
    printList(liste_1);
    
    printf("\n\nRetrait des nombres impairs\n\n");
    removeWhere(liste_1, isOdd);
    printList(liste_1);

    printf("\n\nRetrait des nombres négatifs\n\n");
    removeWhere(liste_1, isNegative);
    printList(liste_1);
    
    freeList(liste_1);
    return 0;
}
