#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "list.h"
//#define NULL ((void*)0)

/**
* Function: createElement
* ----------------------------
*   Create a new Element to use in list.
*   If the malloc fail --> end of the program.
*
* @param value
*    the int value to init in the element 
*
* @return pointer to the new element 
*/
element createElement(int value) {
    element  newElement = (element) malloc( sizeof(struct element_));
    assert( newElement != NULL ); //
    
    newElement->prev = NULL;
    newElement->next = NULL;
    newElement->value = value;
    
    return newElement; 
}

//function return a new list contain 1 element (take the init value in input)
/**
* Function: createList
* ----------------------------
*   Create a new list with 1 element
*   If the malloc fail --> end of the program.
*
* @param value
    the int value to init in the first element 
*
* @return pointer to the list 
*/
list createList(int value) {
    list newList = (list) malloc( sizeof(struct list_));
    assert( newList != NULL );
    
    newList->first = createElement(value);
    newList->last = newList->first;
    
    return newList;
}

/**
* Function: listSize
* ----------------------------
*   Count the number of elements in a list
*
* @param myList
    the list to count
*
* @return the size of the list
*/
int listSize(list myList) {
    int index = 0;
    element elm = myList->first;
    
    for(index = 0; elm != NULL; index++ ) {
        elm = elm->next;
    }
    return index;
}


/**
* Function: listAppend
* ----------------------------
*   Append a new element at the end of a list
*
* @param myList
    the list which to append an element 
*
* @param value
    the value of the new element
*
*/
void listAppend(list myList, int value) {
    element newElement = createElement(value);
    
    myList->last->next = newElement;
    newElement->prev = myList->last;
    myList->last = newElement;

}


/**
* Function: listRemoveElement
* ----------------------------
*   Remove an element of a list (and free the memory)
*   If the position don't exist, the function does nothing
*
* @param myList
    the list which to remove an element 
*
* @param position
    the position of the element to delete
*
*/
void listRemoveElement(list myList, int position) {
    if(position < listSize(myList)) {
        element elm = myList->first;
        
        for(int i = 0; i<position; i++) {
            elm = elm->next;
        }
        //if the element to remove is the first of the list
        if(elm == myList->first) {
            myList->first = elm->next;
            elm->next->prev = NULL;
        }
        //if the element to remove is the lastest of the list
        else if(elm == myList->last) {
            myList->last = elm->prev;
            elm->prev->next = NULL;
        }
        else {
        //make the link betwee the previous and next element
        elm->prev->next = elm->next; 
        elm->next->prev = elm->prev;
        }
        //free the memory
        free(elm);
    }

}



/**
* Function: printList
* ----------------------------
*   print the values of the elements in list
*
*/
void printList(list myList) {
    element elm = myList->first;
    
    for(int index = 0; elm != NULL; index++ ) {
        
        printf("l'element %3d vaut %3d\n",index,elm->value);
        elm = elm->next;
    }
}





//delete and free list
/**
* Function: freeList
* ----------------------------
*   Remove and free all elements and the list
*
* @param myList
    the list to free
*
*/
void freeList(list myList) {
    element elm = myList->first;
    
    while(elm != myList->last) {
            elm = elm->next;
            free(elm->prev);
        }
    //at this moment, the list contain only 1 element
    
    free(elm);
    free(myList);

    }


