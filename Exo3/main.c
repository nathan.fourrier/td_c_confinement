
#include <stdio.h>
#include <stdlib.h>
#include "functions.h"

int main() {
    
    int myIntArray[] = {1,2,3,-4,-8,4,5,6,-5,7,8,9,10,11}; //14 int array
    float myFloatArray[] = {1.3,12.6,0.3,14.0,2.0}; //5 float array
    char myCharArray[] = "dcbafeg"; // 7 char array (+1 whith '\0')
    
    //int
    printf("tableau d'entiers: \n");
    printArrayINT(myIntArray, 14);
    printf("\n\n");    

    sortArray(myIntArray, 14,sizeof(int),compareINT );
    
    printf("tableau d'entiers trié: \n");
    printArrayINT(myIntArray, 14);
    printf("\n\n");
    
    //float
    printf("tableau de flottants: \n");
    printArrayFLOAT(myFloatArray, 5);
    printf("\n\n");    

    sortArray(myFloatArray, 5,sizeof(float),compareFLOAT );
    
    printf("tableau de flottants trié: \n");
    printArrayFLOAT(myFloatArray, 5);
    printf("\n\n");    
    
    //char
    printf("tableau de char: %s \n\n",myCharArray);

    sortArray(myCharArray, 7,sizeof(char),compareCHAR );
    
    printf("tableau de char trié: %s \n",myCharArray);
    

    return 0;
}



/*  for(int i=0; i<14; i++) {
     printf("valeur %2d = %3d\n",i,myArray[i]); //get the element adress starting by the array pointer
 }*/
