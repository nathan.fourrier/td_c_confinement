//print INT array
void printArrayINT(int* array, int length);

//print FLOAT array
void printArrayFLOAT(float* array, int length);

//Compare int by void* given (return 1 if first>second)
int compareINT(void* A, void* B);

//Compare float by void* given (return 1 if first>second)
int compareFLOAT(void* A, void* B);

//Compare char by void* given (return 1 if first>second)
int compareCHAR(void* A, void* B);

//Generic array sort for every type with fucntion pointer
void sortArray(void* array, int length, int elementSize,int (*compare)(void*, void*) );

