#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//printArrayINT
void printArrayINT(int* array, int length) {
    for(int i=0; i<length; i++) {
        printf("Element %d du tableau --> %3d\n",i,array[i]);
    }
}

//printArrayFLOAT
void printArrayFLOAT(float* array, int length) {
    for(int i=0; i<length; i++) {
        printf("Element %d du tableau --> %3f\n",i,array[i]);
    }
}

//compareINT
int compareINT(void* A, void* B) {
    int* A_ = (int*) A;
    int* B_ = (int*) B;
    
    return *A_ > *B_? 1:0;
}

//compareFLOAT
int compareFLOAT(void* A, void* B) {
    float* A_ = (float*) A;
    float* B_ = (float*) B;
    
    return *A_ > *B_? 1:0;
}

//compareCHAR
int compareCHAR(void* A, void* B) {
    char* A_ = (char*) A;
    char* B_ = (char*) B;
    
    return *A_ > *B_? 1:0;
}



void sortArray(void* array, int length, int elementSize,int (*compare)(void*, void*) ) {
    void* tmp = malloc(elementSize);
    void* element;
    void* nextElement;
    
    for(int i=0; i<length; i++) {
        
        for(int j=0; j < (length-1-i); j++) {
        
            element = array + (j * elementSize);
            nextElement = array + (j+1) * elementSize ;
            
            if(compare(element, nextElement)) {
            
                memcpy(tmp,element, elementSize);
                memcpy(element, nextElement, elementSize);
                memcpy(nextElement,tmp, elementSize);
                
            }
        }
    }
    free(tmp);
}


